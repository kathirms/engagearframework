//
//  EngageARFramework.swift
//  EngageARFramework
//
//  Created by kathirms on 26/02/20.
//

import UIKit
import UnityFramework


public class EngageARFramework: NSObject, UnityFrameworkListener {
    
    private var clientID = ""
    private var secretKey = ""
    private weak var ufw:UnityFramework!
    private var window: UIWindow!
    
    internal func validateTheKeys() -> Bool { return (self.clientID == "" || self.secretKey == "") ? false : true }
    
    public static var shared = EngageARFramework()
    
    public func config(clientID: String, secretKey: String, inWindow: UIWindow?) {
        self.clientID = clientID
        self.secretKey = secretKey
        if let win = inWindow {
            self.window = win
            setupUnity(inWindow: self.window)
        }
    }
    
    internal func setupUnity(inWindow: UIWindow?) {
        if inWindow != nil && ufw == nil {
            self.window = inWindow
            let button:UIButton = UIButton(frame: CGRect(x: 0, y: 20, width: 44, height: 44))
            button.setTitleColor(UIColor.white, for: .normal)
            
            //               if let img = FAssetManager.shared.backArrow {
            //                   button.setImage(img, for: .normal)
            //               }
            button.setTitle("back", for: .normal)
            button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
            
            ufw = UnityFramework.getInstance()
            ufw.setDataBundleId("com.unity3d.framework")
            ufw.register(self)
            ufw.runEmbedded(withArgc: CommandLine.argc, argv: CommandLine.unsafeArgv, appLaunchOpts: nil)
            ufw.appController()?.rootView.addSubview(button)
            self.window.makeKeyAndVisible()
        }
    }
    
    @objc func buttonClicked() {
        ufw.sendMessageToGO(withName: "ARSessionOrigin", functionName: "ReleaseAllVideoAllocation", message: "")
        self.window.makeKeyAndVisible()
    }
    
    internal func unloadUnity() {
        if ufw != nil {
            ufw.unloadApplication()
        }
    }
    
    public func unityDidUnload(_ notification: Notification!) {
        if ufw != nil {
            ufw.unregisterFrameworkListener(self)
            self.window.makeKeyAndVisible()
            ufw = nil
        }
    }
    
    public func sendData(sceneName: String, methodName: String, params: String, sceneShow:Bool) {
        if validateTheKeys() {
            if window != nil && ufw != nil {
                ufw.sendMessageToGO(withName: sceneName, functionName: methodName, message: params)
                if sceneShow {
                    ufw.showUnityWindow()
                }
            }else{
                print("Please share the window")
            }
        }else{
            print("Please config the clientID and secretKey")
        }
    }
}
