# EngageARFramework

[![CI Status](https://img.shields.io/travis/Fanisko LLC/EngageARFramework.svg?style=flat)](https://travis-ci.org/Fanisko LLC/EngageARFramework)
[![Version](https://img.shields.io/cocoapods/v/EngageARFramework.svg?style=flat)](https://cocoapods.org/pods/EngageARFramework)
[![License](https://img.shields.io/cocoapods/l/EngageARFramework.svg?style=flat)](https://cocoapods.org/pods/EngageARFramework)
[![Platform](https://img.shields.io/cocoapods/p/EngageARFramework.svg?style=flat)](https://cocoapods.org/pods/EngageARFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EngageARFramework is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EngageARFramework'
```

## Author

Fanisko LLC, kathirms@fanisko.com

## License

EngageARFramework is available under the MIT license. See the LICENSE file for more info.
