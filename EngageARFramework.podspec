#
# Be sure to run `pod lib lint EngageARFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'EngageARFramework'
  s.version          = '1.0.0'
  s.summary          = 'Engage AR is the fans engage platform in sports with augmented reality.'
  #    s.description      = <<-DESC
  #  TODO: Add long description of the pod here.
  #                       DESC
  
  s.homepage         = 'https://gitlab.com/kathirms/engagearframework'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Fanisko LLC' => 'mobileapps@fanisko.com' }
  s.source           = { :git => 'https://gitlab.com/kathirms/engagearframework.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.ios.deployment_target = '12.0'
  s.source_files = 'Source/**/*.swift'
  s.swift_version = '5.0'
  s.platform     = :ios, "12.0"
  
  # s.resource_bundles = {
  #   'EngageARFramework' => ['EngageARFramework/Assets/*.png']
  # }
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
   s.frameworks = 'UnityFramework'
  # s.dependency 'AFNetworking', '~> 2.3'
end
