//
//  AppDelegate.swift
//  EngageARFramework
//
//  Created by Fanisko LLC on 02/26/2020.
//  Copyright (c) 2020 Fanisko LLC. All rights reserved.
//

import UIKit
import EngageARFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var engageAR = EngageARFramework.shared

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        engageAR.config(clientID: "fanisko", secretKey: "12345", inWindow: window)
        return true
    }
}

extension UIViewController {
    weak var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
}
